# Quran Search Telegram Bot

This project is a Telegram bot written in Go. It allows users to search the
Quran with a margin of error in the search query. The bot need to its hosting to
be refreshed pre use to ensure it's online on the free hosting platforms like
[Render](https://render.com).

## Environment Variables

Create a `.env` file in the root of your project and set the following
environment variables:

```env
BOT_TOKEN="<your telegram bot token>"

PING_PONG_URL="<your render.com deployment url(example: https://<repo-name>.onrender.com)>"
```

- `BOT_TOKEN`: The token for your Telegram bot, which you can obtain from the
  BotFather.
- `PING_PONG_URL`: The URL used to ping and keep your bot's hosting service
  alive.

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/sudoCss/quran-search-telegram-bot.git
   cd quran-search-telegram-bot
   ```

2. Install dependencies:
   ```bash
   go mod tidy
   ```

3. Set up the environment variables in a `.env` file as described above.

## Usage

To start the bot, run:

```bash
go run cmd/bot/main.go
```

The bot will start and listen for messages on Telegram. Also the Ping listener
will start and listen for any request on any rout.

## Contributing

If you would like to contribute to this project, please fork the repository and
submit a merge request. All contributions are welcome!

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file
for details.
