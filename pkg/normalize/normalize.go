package normalize

import (
	"strings"

	"golang.org/x/text/unicode/norm"
)

func Normalize(text string) string {
	// Remove diacritics (حركات) using NFKD normalization form
	normalized := norm.NFKD.String(text)

	normalized = strings.Replace(normalized, "أ", "ا", -1)
	normalized = strings.Replace(normalized, "أ", "ا", -1)

	normalized = strings.Replace(normalized, "إ", "ا", -1)
	normalized = strings.Replace(normalized, "إ", "ا", -1)

	normalized = strings.Replace(normalized, "آ", "ا", -1)
	normalized = strings.Replace(normalized, "آ", "ا", -1)

	normalized = strings.Replace(normalized, "ى", "ي", -1)

	normalized = strings.Replace(normalized, "ة", "ه", -1)

	return normalized
}
