package search_test

import (
	"testing"

	"gitlab.com/sudoCss/quran-search-telegram-bot/pkg/search"
)

func TestSearch(t *testing.T) {
	res := search.Search("تدانيتم", 2, 5)
	if res[0].ID != 289 {
		t.Errorf("expected top result to be aya with id 289, but found aya with id %d", res[0].ID)
	}

	res = search.Search("تدانيتم", 1, 5)
	if len(res) != 0 {
		t.Errorf("expected 0 results but found %d results", len(res))
	}

	res = search.Search("تداينتم", 0, 5)
	if len(res) != 1 {
		t.Errorf("expected only one result but found %d results", res[0].ID)
	}
	if res[0].ID != 289 {
		t.Errorf("expected the only result to be aya with id 289, but found aya with id %d", res[0].ID)
	}
}
