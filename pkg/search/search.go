package search

import (
	"sort"

	"gitlab.com/sudoCss/quran-search-telegram-bot/pkg/normalize"
	"gitlab.com/sudoCss/quran-search-telegram-bot/pkg/quran"
	"golang.org/x/text/unicode/norm"
)

type SearchItem struct {
	Ayah     quran.Aya
	Distance int
}

func levenshteinDistance(a, b string) int {
	la := len(a)
	lb := len(b)

	if la == 0 {
		return lb
	}
	if lb == 0 {
		return la
	}

	matrix := make([][]int, la+1)
	for i := range matrix {
		matrix[i] = make([]int, lb+1)
	}

	for i := 0; i <= la; i++ {
		matrix[i][0] = i
	}
	for j := 0; j <= lb; j++ {
		matrix[0][j] = j
	}

	for i := 1; i <= la; i++ {
		for j := 1; j <= lb; j++ {
			cost := 0
			if a[i-1] != b[j-1] {
				cost = 1
			}

			matrix[i][j] = min(
				matrix[i-1][j]+1,
				min(matrix[i][j-1]+1, matrix[i-1][j-1]+cost),
			)
		}
	}

	return matrix[la][lb]
}

func searchAyahs(ayahs []quran.Aya, query string, distanceThreshold int) []quran.Aya {
	normalizedQuery := normalize.Normalize(query)
	queryLength := len(normalizedQuery)

	results := []SearchItem{}

	for _, ayah := range ayahs {
		normalizedText := ayah.AyaTextEmlaeyNormalized
		textLength := len(normalizedText)
		minDistance := len(normalizedQuery) + len(normalizedText) // Max possible distance

		for i := 0; i <= textLength-queryLength; i++ {
			substring := normalizedText[i : i+queryLength]
			distance := levenshteinDistance(normalizedQuery, substring)
			if distance < minDistance {
				minDistance = distance
			}
		}

		if minDistance <= distanceThreshold {
			results = append(results, SearchItem{ayah, minDistance})
		}
	}

	sort.Slice(results, func(i, j int) bool {
		return results[i].Distance < results[j].Distance
	})

	matches := []quran.Aya{}
	for _, result := range results {
		matches = append(matches, result.Ayah)
	}

	return matches
}

func containsNonArabic(text string) bool {
	normalized := norm.NFKD.String(text)

	arabicRange := []struct {
		start rune
		end   rune
	}{
		{0x0600, 0x06FF}, // Arabic
		{0x0750, 0x077F}, // Arabic Supplement
		{0x08A0, 0x08FF}, // Arabic Extended-A
		{0xFB50, 0xFDFF}, // Arabic Presentation Forms-A
		{0xFE70, 0xFEFF}, // Arabic Presentation Forms-B
	}

	isArabic := func(r rune) bool {
		if r == ' ' {
			return true
		}

		for _, rng := range arabicRange {
			if r >= rng.start && r <= rng.end {
				return true
			}
		}

		return false
	}

	for _, r := range normalized {
		if !isArabic(r) {
			return true
		}
	}

	return false
}

func Search(query string, threshold, topK int) []quran.Aya {
	if containsNonArabic(query) {
		return []quran.Aya{}
	}

	matches := searchAyahs(quran.Quran[:], query, threshold)
	if topK < len(matches) {
		return matches[:topK]
	}
	return matches
}
