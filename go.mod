module gitlab.com/sudoCss/quran-search-telegram-bot

go 1.22.3

require (
	github.com/joho/godotenv v1.5.1
	golang.org/x/text v0.3.7
	gopkg.in/telebot.v3 v3.2.1
)
