package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
	"unicode/utf8"

	"github.com/joho/godotenv"
	"gitlab.com/sudoCss/quran-search-telegram-bot/pkg/search"
	telebot "gopkg.in/telebot.v3"
)

func init() {
	if os.Getenv("CURRENT_ENV") != "prod" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	if os.Getenv("BOT_TOKEN") == "" || os.Getenv("PING_PONG_URL") == "" {
		log.Fatal("Missing some required environment variables, see README.md")
	}
}

func main() {
	pref := telebot.Settings{
		Token:  os.Getenv("BOT_TOKEN"),
		Poller: &telebot.LongPoller{Timeout: 10 * time.Second},
	}

	b, err := telebot.NewBot(pref)
	if err != nil {
		log.Fatal(err)
		return
	}

	b.Handle("/start", func(c telebot.Context) error {
		c.Send(fmt.Sprintf(`بوت للبحث في القرآن الكريم (يدعم وجود اخطاء بسيطة في البحث المطلوب)
البوت مرفوع على استضافة مجانية, قد يتم ايقافها من قبل المزود في حال عدم وجود نشاط على البوت لمدة معينة.. في حال لم يستجب البوت يرجى الضغط على الزر الازرق الموجود اسفل المحادثة(أو على الرابط التالي: %s) والانتظار حتى تفتح صفحة الويب وتظهر لك رسالة تفيد بأن البوت عاد للعمل`, os.Getenv("PING_PONG_URL")))

		return c.Send(`ارسل عبارة البحث باللغة العربية حصراً, البوت يتبع خوارزمية Levenshtein Distance لإيجاد آيات تحوي نص مقارب لعبارة البحث المطلوبة ويتم ترتيب النتائج حسب القرب وإرسال النتائج الخمسة الأولى فقط!`)
	})

	b.Handle(telebot.OnText, func(c telebot.Context) error {
		query := c.Message().Text
		if utf8.RuneCountInString(query) < 3 || utf8.RuneCountInString(query) > 100 {
			return c.Reply("للحصول على نتائج جيدة يرجى أن يكون طول عبارة البحث بين 3 - 100 حروف(لا مشكلة بوجود مسافة فارغة كذلك)")
		}

		c.Reply("انتظر قليلاً من فضلك...")
		res := search.Search(query, 2, 5)
		if len(res) == 0 {
			return c.Reply("لا يوجد نتائج قريبة من عبارة البحث المدخلة")
		}

		for i, r := range res {
			c.Reply(fmt.Sprintf(`%d %s - الآية: { %v } %s - الآية لسهولة القراءة: { %v } %s [Sura %s, Aya %d]/[سورة %s, آية %d]`, i+1, "\n", r.AyaText, "\n", r.AyaTextEmlaey, "\n", r.SuraNameEn, r.AyaNo, r.SuraNameAr, r.AyaNo))
		}
		return c.Reply("انتهى")
	})

	go pingPong()

	b.Start()
}

func pingPong() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "bot is online")
	})

	fmt.Println("ping pong server on :8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
