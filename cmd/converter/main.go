package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/sudoCss/quran-search-telegram-bot/pkg/normalize"
)

type Aya struct {
	ID            int    `json:"id"`
	Jozz          int    `json:"jozz"`
	Page          int    `json:"page"`
	SuraNo        int    `json:"sura_no"`
	SuraNameEn    string `json:"sura_name_en"`
	SuraNameAr    string `json:"sura_name_ar"`
	LineStart     int    `json:"line_start"`
	LineEnd       int    `json:"line_end"`
	AyaNo         int    `json:"aya_no"`
	AyaText       string `json:"aya_text"`
	AyaTextEmlaey string `json:"aya_text_emlaey"`
}

const header = `// Programmatically generated with /cmd/converter/main.go

package quran

type Aya struct {
	ID            			int
	Jozz          			int
	Page          			int
	SuraNo        			int
	SuraNameEn    			string
	SuraNameAr    			string
	LineStart     			int
	LineEnd       			int
	AyaNo         			int
	AyaText       			string
	AyaTextEmlaey 			string
	AyaTextEmlaeyNormalized string
}

var Quran [6236]Aya = [6236]Aya{
`
const footer = `}
`

func main() {
	file, err := os.Open("./assets/json/hafsData_v2-0.json")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	bytes, err := io.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	var ayahs []Aya
	err = json.Unmarshal(bytes, &ayahs)
	if err != nil {
		log.Fatal(err)
	}

	if err := os.RemoveAll("./pkg/quran/"); err != nil {
		log.Fatal(err)
	}

	if err := os.MkdirAll("./pkg/quran/", os.ModePerm|os.ModeDir); err != nil {
		log.Fatal(err)
	}

	pkg, err := os.OpenFile("./pkg/quran/quran.go", os.O_CREATE|os.O_RDWR, os.ModeAppend|os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
	defer pkg.Close()

	pkg.WriteString(header)
	for _, aya := range ayahs {
		pkg.WriteString(fmt.Sprintf(`	{ID: %d, Jozz: %d, Page: %d, SuraNo: %d, SuraNameEn: "%v", SuraNameAr: "%v", LineStart: %d, LineEnd: %d, AyaNo: %d, AyaText: "%v", AyaTextEmlaey: "%v", AyaTextEmlaeyNormalized: "%v"},%s`, aya.ID, aya.Jozz, aya.Page, aya.SuraNo, aya.SuraNameEn, aya.SuraNameAr, aya.LineStart, aya.LineEnd, aya.AyaNo, aya.AyaText, aya.AyaTextEmlaey, normalize.Normalize(aya.AyaTextEmlaey), "\n"))
	}
	pkg.WriteString(footer)
}
